import 'dart:math';
import 'package:flutter/foundation.dart';

import 'remote_provider.dart';
import 'remote_provider_data.dart';

/// Constants for stub API
abstract class StubApiConst {
  /// Lower delay bound.
  static const lowerBound = 300;
  /// Upper delay bound.
  static const upperBound = 600;
}

/// Stub Implementation for API
class RemoteStubProviderImpl implements RemoteProvider {
  Future<String> fetch() async {
    return _isolatedFetch();
  }
}

/// Randomizer for delay emulation.
final _random = new Random();

/// Next time slot for delay emulation.
int _next(int min, int max) => min + _random.nextInt(max - min);

Future <String> _isolatedFetch() async {
  return compute(_loadFromAsset, '');
  //return await _loadFromAsset();
}

/// Loading emulation.
Future<String> _loadFromAsset([String dumb]) async {
  final start = DateTime.now().millisecondsSinceEpoch;
  final s = TheData;
  final passed = DateTime.now().millisecondsSinceEpoch - start;
  final time = _next(StubApiConst.lowerBound, StubApiConst.upperBound - passed);
  await Future.delayed(Duration(milliseconds: time));
  return s;
}
