# Пример flutter-проекта

Работа сделана согласно [ТЗ](https://gitlab.com/frostyland/flutter_sample1/-/blob/master/docs/tz/tz1.pdf).

Версия Flutter: **Flutter (Channel stable, 1.20.2, on Linux, locale en_US.UTF-8)**

Версия Dart: **Dart SDK version: 2.9.1 (stable) (Wed Aug 12 12:37:19 2020 +0200) on "linux_x64"**

Проверка для iOS сделана при помощи CodeMagic -> VNC -> виртуальной машины iOS с XCode 11.6.

**Android:**

<p float="left">
    <img src="./images/android_1.jpg" alt="drawing" width="150"/>
    <img src="./images/android_2.jpg" alt="drawing" width="150"/>
    <img src="./images/android_3.jpg" alt="drawing" width="150"/>
    <img src="./images/android_4.jpg" alt="drawing" width="150"/>
    <img src="./images/android_5.jpg" alt="drawing" width="150"/>
</p>    

**iOS:**

<p float="left">
    <img src="./images/ios_1.png" alt="drawing" width="150"/>
    <img src="./images/ios_2.png" alt="drawing" width="150"/>
    <img src="./images/ios_3.png" alt="drawing" width="150"/>
    <img src="./images/ios_4.png" alt="drawing" width="150"/>
    <img src="./images/ios_5.png" alt="drawing" width="150"/>
</p>    
